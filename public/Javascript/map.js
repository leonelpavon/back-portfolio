var map = L.map('main_map').setView([-34.760578, -58.401061], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

L.marker([-34.760215, -58.404072]).addTo(map), 
L.marker([-34.757729, -58.403557]).addTo(map)